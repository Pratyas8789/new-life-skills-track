# Elasticsearch
Elasticsearch is a free, open-source search and analytics engine based on the Apache Lucene library. It’s the most popular search engine and has been available since 2010. It’s developed in Java, supporting clients in many different languages, such as PHP, Python, C#, and Ruby.Data is serialized in JSON format.

Elasticsearch is commonly used for full-text search, log analysis, and business analytics. It can be used to index and search structured and unstructured data, such as text, geo-spatial data, and time-series data. Elasticsearch is also highly scalable and can be used in a cluster of nodes to distribute the workload and provide high availability.

Elasticsearch is a part of the Elastic Stack, which includes Kibana for data visualization, Logstash for data ingestion, and Beats for lightweight data shippers. Together, these tools provide a comprehensive solution for collecting, analyzing, and visualizing data.

{

  "first_name": "Pratyas",

  "last_name":"kumar",

  "email":"xyz@gmail.com",

  "dob":"21-06-2001",

  "city":"Gopalganj",

  "state":"Bihar",

  "country":"India",

  "occupation":"Software Engineer"

}
# Solr
Solar stands for Searching on Luncene Replication. It is a free open source search engine based on the Apche Luncene library.
Search Engines mainly work as filters for the need of information available on the World Wide Web. It allows us to quickly and easily find any information about their interest or value, without the need to go through numerous irrelevant web pages. The goal of the Search Engines is to give users filtered search results that lead to relevant information on high-quality websites where a huge number of data is available, such as- JavaTpoint, Wikipedia, etc.

Solr is a full-text search platform that supports a variety of search features, including faceting, hit highlighting, spell checking, and more. It also includes a rich set of indexing features that can be used to index a variety of content types, including structured and unstructured data.Solr is highly scalable and can be used in a cluster of nodes to provide high availability and distribute the workload. It also provides a RESTful API that can be used to interact with the search engine, making it easy to integrate with other applications.

Solr is widely used in a variety of applications, including e-commerce, media and publishing, and enterprise search. It is often used alongside other tools in the Apache Solr ecosystem, such as Apache Nutch for web crawling and Apache Tika for content extraction.
# Lucene
Lucene is a full-text search library in Java which makes it easy to add search functionality to an application or website.
It does so by adding content to a full-text index. It then allows you to perform queries on this index, returning results ranked by either the relevance to the query or sorted by an arbitrary field such as a document's last modified date.

Lucene is able to achieve fast search responses because, instead of searching the text directly, it searches an index instead. This would be the equivalent of retrieving pages in a book related to a keyword by searching the index at the back of a book, as opposed to searching the words in each page of the book.Lucene provides a powerful set of indexing and searching features, including support for Boolean queries, phrase queries, wildcard queries, and fuzzy queries. It also provides support for faceted search, which allows users to filter search results based on specific criteria.

Lucene is widely used in combination with other search engines such as Solr and Elasticsearch. These search engines build on top of Lucene and provide additional features such as distributed searching and indexing, advanced query parsing, and real-time indexing.


## References:
* https://www.geeksforgeeks.org/elasticsearch-search-engine-an-introduction/ 
* https://www.lucenetutorial.com/basic-concepts.html#:~:text=Lucene%20is%20a%20full%2Dtext,to%20a%20full%2Dtext%20index
* https://sematext.com/guides/solr/#:~:text=Apache%20Solr%20