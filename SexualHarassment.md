# Prevention of Sexual Harassment

## *What kinds of behaviour cause sexual harassment?*

* Involving yourself more in someone's personal life may disturb them.

* Making offensive remarks about an individual or a group of individuals can poison the workplace.

* Constantly staring at someone in an inappropriate manner.

* Touching someone in an inappropriate way is also considered sexual harassment.

* A employee sends another coworker sexually explicit messages or photographs.


## *What would you do in case you face or witness any incident or repeated incidents of such behaviour?*

* Confronting the person causing such behavior and asking them to stop such behavior.

* Informing your close friends about such behavior and asking them for help to deal with such situations.

* Informing the concerned authority about such an event and asking for strict action for such behavior.

* Get sufficient proof of such improper behaviour so that I can present it to the appropriate authorities.

* Getting help from sexual harassment prevention NGOs and government institutionss.

             

