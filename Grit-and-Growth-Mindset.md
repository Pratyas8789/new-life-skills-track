 # Grit

## *Paraphrase (summarize) the video in a few lines. Use your own words.*

Ans:- Speaker believes that IQ is not the only factor in determining a student's success in school and life. She conducted various studies and found that grit, which she defines as passion and perseverance for long-term goals. Grittier individuals are more likely to succeed even when matched on other measurable characteristics. However, little is known about how to build grit in individuals, especially in children. She suggests that a growth mindset, the belief that one's abilities can change with effort, is a good start but more research and testing are needed to determine effective ways to develop grit.

## *What are your key takeaways from the video to take action on?*

Ans:- 
* IQ alone is not the only difference between the best and worst students.
* Every student can learn if they work hard and long enough.
* Grit is important in education, particularly for students who are not performing well.



# Introduction to Growth Mindset

## *Paraphrase (summarize) the video in a few lines in your own words.*

Ans:- People with a fixed mindset believe that skills are inherent, whereas people with a growth mindset believe that skills can be developed. People with a fixed mindset do not believe in learning and growth, while those with a growth mindset believe in their capacity to learn and grow. Mindsets have a significant impact on a person's ability to learn. The growth mindset is the foundation for learning, and its power is being implemented in companies, sports teams, and schools worldwide. The fixed and growth mindsets have significant differences in terms of beliefs and focus. The fixed mindset avoids the four key ingredients to growth, which are effort, challenges, mistakes, and feedback, while the growth mindset embraces them as opportunities to learn and improve. 




## *What are your key takeaways from the video to take action on?*

Ans:- 
* People with a fixed mindset believe that skills are born, while people with a growth mindset believe that skills are built.
* People with a growth mindset believe in their capacity to learn and grow, while fixed mindset do not belive that.
* Growth mindset is the foundation for learning, and it is a powerful concept helping many people and organizations around the world.


# Understanding Internal Locus of Control

## *What is the Internal Locus of Control? What is the key point in the video?*

Ans:- The internal locus of control is a concept that refers to the extent to which a person believes they have control over their life. In this video, a group of fifth graders were given challenging puzzles to solve. Half of the students were told they scored well because they were smart, while the other half were told it was because they worked hard. The results showed that the students who believed they did well because of their hard work had a higher level of motivation and enjoyed the experience more. This is an indication of an internal locus of control. On the other hand, the students who were told they did well because they were smart had lower motivation levels, spent most of their time on easy puzzles, and did not enjoy the experiment as much. 

*There are some key points:-*

* An internal locus of control means believing that factors you control led to your outcomes.

* People with an external locus of control believe factors outside of what they could control were responsible for their success or failure, leading to lower motivation.

* To adopt an internal locus of control, solving problems in my life, and appreciate the effort I put in to solve them.


# How to build a Growth Mindset

## *Paraphrase (summarize) the video in a few lines in your own words.*

Ans:- The video discusses how to develop a growth mindset. Many people have a fixed mindset, which means they don't believe they can improve or grow. There are three things : first, believe in your ability to figure things out. This means believing that you can get better and improve in life. The second things is that people often assume that they can't do something, but this kind of thinking limits their growth. The third thing is that to develop your own life curriculum. This means seeking out knowledge and resources to help you achieve your goals. By doing these things, you can develop a growth mindset and be more successful in life.

## *What are your key takeaways from the video to take action on?*

Ans:- 
* To develop a growth mindset, the fundamental rule is to believe in your ability to figure things out and get better.

* A fixed mindset is where people don't believe they can grow or get better.

* The growth mindset is the belief that one can get better and improve.

* Developing a life curriculum is crucial for long-term growth as you should learn new things that align with your passion and dreams.

# Mindset - A MountBlue Warrior Reference Manual

## *What are one or more points that you want to take action on from the manual? (Maximum 3)*

Ans:- 
* I will stay relaxed and focused no matter what happens.

* I know more efforts lead to better understanding.
