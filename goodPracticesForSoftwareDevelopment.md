# Good Practises for Software Developer

## *What is your one major takeaway from each one of the 6 sections ?*
Ans:-
1. getting a clear understanding of the client Software development starts with requirements, so it is important to clearly document them and have the customer or the assigning authority confirm their accuracy before moving further. This will ensure that everyone is on the same page.

2. When the team and you are working on the project Since other team members may be depending on our efforts, communication with the team members at the appropriate moment is crucial. Hence, informing them of any delays or deadlines that we may miss will allow them to adjust the schedule.

3. When we are stuck, it is necessary to seek for assistance, but how we ask for assistance is just as crucial. The proper technique is to describe the difficulties using numerous tools, including code snippets, screenshots, screen captures, and logs, which can aid in the person's understanding of the problems.

4. Knowing your coworkers may greatly aid in increasing productivity while working in a team because there won't be any communication breakdowns that would slow down the flow of information.

5. We should be careful while speaking with our teammates since too little communication can result in unhealthy relationships amongst teammates while too much conversation can make people feel uncomfortable.

6. In order to be successful at programming, you must be engaged and focused. To make the most of our time and achieve the best results, we should make every effort to limit distractions.



## *Which area do you think you need to improve on? What are your ideas to make progress in that area?*
Ans:- 
I believe I need to work on more effective approaches to validate the requirements that have been documented for the project. I'll use a notepad to jot down the needs and have those details confirmed during the actual meeting.

Keeping a healthy conversation with my teammate might help me increase my productivity by tackling issues quicker. I can try to schedule daily meetings with my teammates.