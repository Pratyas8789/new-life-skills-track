# **Energy Management**

# Manage Energy not Time

## *What are the activities you do that make you relax - Calm quadrant ?*

* A brief period of meditation has helped me relax and calm my mind.

* Every evening, I go for a walk to warm up and to clear my mind of the numerous thoughts that race through it.

* I can lower my stress and concentrate on things that are more important by having good conversations with my family and friends.

* I hear my favourite music, which calms my mind.


## *When do you find getting into the Stress quadrant?*

* When I have a lot of stuff to do and a short deadline, I become anxious.

* When I can't complete a task perfectly, I become anxious.

* when I observe someone performing jobs inappropriately I become angry


## *How do you understand if you are in the Excitement quadrant?*

* I feel satisfied when I finish my assignment on schedule.

* I get excited whenever friends surprise me with a visit.

* Traveling with my friends and family makes me happy.


# Sleep is your superpower

## *Paraphrase the Sleep is your Superpower video in detail.*

* Both before and after learning anything, sleep is crucial to the learning process. Prior to learning something new, getting enough sleep prepares our brain to take in more information.

* The speaker's research indicates that those with regular sleep patterns are 40% more productive when learning.

* Limiting our sleep to 4 hours can result in a 75% decrease in natural killer cells, which in fact help our body resist sickness.

* Establishing a regular sleep routine can improve our sleep.

* Maintaining a low body temperature as well as the ambient temperature can promote rapid sleep.


## *What are some ideas that you can implement to sleep better?*

* Reducing my caffeine intake.

* Consistently engaging in prayer and meditation.

* Reducing the amount of food consumed before bed.

* If I don't use my phone immediately before going to bed, I can get to bed early.

* If I establish a consistent bedtime, I can receive good-quality sleep.

# Brain Changing Benefits of Exercise

## *Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.*


* The speaker shares her experience of going on a kayaking trip by herself and learning that she was the worst person among the group of individuals she went with. She then made the decision that she would never act badly again.


* She got to work on her health and began to frequently exercise; she also tried yoga, Zumba, kickboxing, and every other form of exercise that would help her reach her health objectives.


* Excersing, according to literature, is said to improve our moods, vitality, memory, and attention. She noticed all these changes in herself.


* Our prefrontal cortex and hippocampus can expand and become stronger as a result of exercise.


* The minimum amount of time we should devote to exercise, according to the speaker, is 30 minutes, at least four times per week. Moreover, we don't have to visit the gym, power walking can be done as a part of exercise.


## *What are some steps you can take to exercise more?*

* Attempt to be more physically active and complete all of my daily tasks on my own, without assistance.

* Making at least 15 to 20 minutes of exercise a part of your daily schedule.

* When travelling small distances, prefer to walk rather than use a vehicle.

* Exercising with a friend can help keep me motivated and make my workouts more enjoyable.