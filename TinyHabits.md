# Tiny Habits

## *Your takeaways from the video (Minimum 5 points)*
Ans:-
* We need to develop any habits that help us stay healthy or peaceful, such as doing pushups after doing anything else and celebrating when our work is done.
* Set the aspiration clearly.
* Pick a suitable prompt.
* Complement with particular behaviours.
* Begin small.

## *Your takeaways from the video in as much detail as possible*
Ans:-
* Behavior is a function of Motivation, Ability, and Prompt.
* Low motivation is needed for simple tasks and vice versa.
* Establishing a habit is simple, but maintaining it over time is challenging.
* The answer is to reduce the habit to its most minute form. This makes it simple to adopt and establish permanently.

* The finest method for forming a new habit is the action prompt. This prompt uses our current momentum to propel a new, minor endeavour.

* The most important aspect of developing a habit is learning how to celebrate after a task is completed. It maintains the desire to do the habit again.

* when we succeed in our goals. Our motivation and self-assurance levels rise. This is the moment of success that is influenced by the frequency of successes rather than the magnitude of the achievement.

## *How can you use B = MAP to make making new habits easier?*
Ans:-
* I'll make my bed as soon as I get out of bed.
* I'll do five push-ups when I've finished cleaning my teeth.
* I'll take a brief stroll after finishing my dinner.
* I'll consider one positive event from the day before going to bed.
* I'll get tools and resources that help me with the new habit.

## *Why it is important to "Shine" or Celebrate after each successful completion of habit?*
Ans:-
It is important to "shine" or celebrate after each successful completion of a habit because it reinforces the behavior and creates a positive feedback loop in the brain. The brain likes to repeat actions that are associated with positive emotions and rewards, so celebrating the completion of a habit makes it more likely that you will continue to perform that habit in the future. Celebrating also helps to build self-confidence and self-efficacy, making it easier to tackle bigger challenges and goals. Additionally, celebrating can help to make the habit more enjoyable, which makes it easier to maintain in the long run.


## *Your takeaways from the video (Minimum 5 points)*
Ans:-
* The effects of habits are compounded; initially, they are not very noticeable, but over time, we see a significant improvement in ourselves.
* Don't just create goals if you want greater results; instead, concentrate on the steps necessary to accomplish them.
* Focusing on how to reach your goals rather than what you want to do can help you modify your behaviours the most.
* Make it evident, appealing, simple, and fulfilling if you want to develop positive habits.
* Achievement comes from regular routines, not abrupt shifts in behaviour. Making a decision on an especially motivational day won't alter anything the following day since you might forget about it.
Obtaining 1 percent


## *Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?*
Ans:-
* We use a different approach to creating habits. We attempt to form a habit and then hope that after engaging in this behaviour for a sufficient amount of time. It'll become a part of who we are.
* Instead, the identity should come first in our minds. For instance, if I want to get healthy, I should consider myself fit and eat healthily, avoid frequent consumption of junk food, and exercise frequently.

## *Write about the book's perspective on how to make a good habit easier?*
Ans:-
Making a positive habit easier requires:-

* Obviouss
* Attractives
* Easy
* Satisfying

We must place fewer barriers between ourselves and good behaviours in order to make good habits simpler.

Make habits desirable.

To make something simple is to lessen resistance and to set up our surroundings for the habits we want to establish. It is far more likely to accomplish the task when the friction is reduced.

In order to create habits that are instantly fulfilling, we should aim to associate some kind of instant gratification.

## *Write about the book's perspective on making a bad habit more difficult?*
Ans:-
* Make it undetectable.
* Make it look bad.
* Harden it up.
* Make it insufficient.


## *Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?*
Ans:-
* I will adopt the habit of eating healthy only from now on.
* I will create my identity as a healthy person. I will not try to eat junks everyday. May be once or twice a month.

## *Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?*

Ans:- 
* I'll stop using my phone right before going to bed.
* I'll put the phone in my backpack before retiring for the night. I would attempt to read a few Kindle pages.
* I would persuade myself by reading some articles about the drawbacks of using a phone when lying in bed.